@extends('layouts.app')
@section('title', 'Home')
@section('content')

<section class="landing">
	<div class="container">
		<div class="caption">
			<p class="small">AFFORDABLE, ETHICAL, ENABLING</p>
			<h1>
				Welcome to a whole new way of buying, selling and leasing an affordable home
			</h1>
			<h4>Working with housing associations, we’ll help you buy an affordable home.</h4>
			<div class="view">
				<button class="btn"><a href="/posts">View Available</a></button>
			</div>

			<div class="reg-log row justify-content-center">

				<div class="login">
					@guest
					<button class="btn">
						<a href="/login">Login</a>
					</button>
				</div>

				<div class="register">
					@if (Route::has('register'))
					<button class="btn">
						<a href="/register">Register</a>
					</button>
					@endif
					@endguest
				</div>

			</div>
		</div>

	</div>
</section>

@endsection
