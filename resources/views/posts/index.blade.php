@extends('layouts.app')
@section('title', 'All Posts')
@section('content')

<div class="posts">

	<section class="banner">
		@include('inc.navbar')
		<div class="caption">
			<h1>
				If you’re a buyer,seller or just looking to lease,<br> don’t struggle to find your asset
			</h1>
		</div>
	</section>

	<div class="container">
		<div class="create">
			<a href="/post_create">Add new Post</a> <br>
		</div>
	</div>

	<section>
		@if(count($posts)>0)
		@foreach($posts as $post)


		<div class="container">
			<div class="row">
				<div class="col-lg-7 view">
					<img src="{{$post->image}}" alt="img">

				</div>

				<div class="col-lg-5 details">
					<h4><a href="/post_view/{{$post->id}}">Title: {{$post->title}}</a></h4>
					<h5>Category: {{$post->category->name}}</h5>
					<p>Location: {{$post->location->name}}</p>
					<h5>Price: {{$post->price}}</h5>
					<small>Posted on: {{$post->created_at->format("M,d,Y")}}</small>

					<small>Posted by: {{$post->user->name}}</small>

					<div class="func">
						<a class="view-more" href="/post_view/{{$post->id}}">View more details</a> <br>
						<div class="edit-delete">
							@if(!Auth::guest())
							@if(Auth::user()->id == $post->user_id)
							<a class="nav-link edit" href="/post_edit/{{$post->id}}">Edit</a>
							<form action="/post_delete/{{$post->id}}" method="POST">
								@csrf
								@method('DELETE')
								<button class="btn delete" type="submit">Delete</button>
							</form>
							@endif
							@endif
						</div>

					</div>
				</div>

			</div>

			<hr class="line">



		</div>
		@endforeach
		{{$posts->links()}}
		@else
		<p>Start adding a Post</p>
		@endif

	</section>

</div>
@endsection
