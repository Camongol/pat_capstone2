@extends('layouts.app')
@section('title', 'View Posts')
@section('content')

<section class="view-post">
	@include('inc.navbar')

	<div class="container">

		<div class="options">
			<a href="/posts">View all</a>
			<a href="/dashboard">Dashboard</a>
		</div>

		<div class="row">
			<div class="col-lg-6">
				<img src="{{$post->image}}" alt="image">
			</div>
			<div class="col-lg-6 details">

				<h4>Title: {{$post->title}}</h4>
				<h5>Category : {{$post->category->name}}</h5>
				<p>Location: {{$post->location->name}}</p>
				<p>Price: {{$post->price}}</p>
				<p>Description {{$post->description}}</p>
				<small>Posted on: {{$post->created_at->format("M,d,Y")}}</small>
				<small>by: {{$post->user->name}}</small>

				<div class="func">
					<div class="edit-delete">


						@if(!Auth::guest())
						@if(Auth::user()->id == $post->user_id)
						<a class="nav-link edit" href="/post_edit/{{$post->id}}">Edit</a>

						<form action="/post_delete/{{$post->id}}" method="POST">
							@csrf
							@method('DELETE')
							<button class="btn delete" type="submit">Delete</button>
						</form>
					</div>
				</div>
				@endif
				@endif

			</div>
		</div>
	</div>

</section>


@endsection
