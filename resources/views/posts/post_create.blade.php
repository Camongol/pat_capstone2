@extends('layouts.app')
@section('title', 'Create Post')
@section('content')

<section class="new-post">
	@include('inc.navbar')
	<div class="banner">
		<div class="container">

			<div class="col-lg-8 offset-lg-2">
				{!! Form::open([
				'action' => 'PostController@store',
				'method' =>'POST',
				'enctype' => 'multipart/form-data'
				]) !!}

				<h2>Create new Post</h2>

				<div class="form-group">
					<label for="title">Title</label>
					<input type="text" name="title" class="form-control">
				</div>

				<div class="form-group">
					<label for="location_id">Location</label>
					<select name="location_id" class="form-control">Choose Location
						@foreach($locations as $location)
						<option value="{{$location->id}}">{{$location->name}}</option>
						@endforeach
					</select>

				</div>

				<div class="form-group">
					<label for="category_id">Choose Category</label>
					<select name="category_id" class="form-control">
						@foreach($categories as $category)
						<option value="{{$category->id}}">{{$category->name}}</option>
						@endforeach
					</select>
				</div>

				<div class="form-group">
					<label for="description">Description</label>
					<textarea name="description" class="form-control"></textarea>
				</div>

				<div class="form-group">
					<label for="price">Price</label>
					<input type="text" name="price" class="form-control">
				</div>
				<div class="form-group">
					<label for="image">Upload Image:</label>
					<input type="file" name="image" class="form-control">
				</div>
				<div>
					<button type="submit" class="btn">Submit</button>
				</div>

				{!! Form::close()!!}
			</div>
		</div>
	</div>
</section>
@endsection
