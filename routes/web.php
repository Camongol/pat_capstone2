<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/dashboard', 'DashboardController@index');

Route::get('posts', 'PostController@index');
Route::get('/post_view/{id}', 'PostController@show');

Route::get('/post_create', 'PostController@create');
Route::post('/post_create', 'PostController@store');

Route::get('/post_edit/{id}', 'PostController@edit');
Route::patch('/post_edit/{id}', 'PostController@update');

Route::delete('/post_delete/{id}', 'PostController@destroy');