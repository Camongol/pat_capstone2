<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use App\Category;
use App\Location;
use App\Status;
use App\User;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['except' =>'index']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('created_at','desc')->paginate(4);
        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $locations = Location::all();
        $statuses = Status::all();
        return view('posts.post_create',compact('categories','locations','statuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate and create post here..
        $this->validate($request,[
            'title' => 'required',
            'category_id' => 'required',
            'location_id' => 'required',
            'description' => 'required',
            'price' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg,|max:1500'
        ]);

        //image upload
        //time() gets the current time
        //getClientOriginaltExtension gets file extension

        $image = $request->file('image');
        $image_name = time().".".$image->getClientOriginalExtension();
        $destination = "images/"; //corresponds to /public/images
        $image->move($destination, $image_name);
        
        $newPost = new Post;
        $newPost ->title = $request->title;
        $newPost ->category_id = $request->category_id;
        $newPost ->location_id = $request->location_id;
        $newPost ->user_id = auth()->user()->id;
        $newPost ->description = $request->description;
        $newPost ->price = $request->price;
        $newPost->image = $destination.$image_name;
        $newPost ->status_id = 1;

        $newPost->save();
        return redirect('posts')->with('success', 'New Post Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        return view ('posts.post_view', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

      
        //check for correct user

        if(auth()->user()->id !==$post->user_id){
            return redirect('/posts')->with('error', 'Unauthorised Page');
        }

        $categories = Category::all();
        $locations = Location::all();
        $statuses = Status::all();



        return view
        ('posts.post_edit',compact('post','categories','locations','statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required',
            'category_id' => 'required',
            'location_id' => 'required',
            'description' => 'required',
            'price' => 'required'
        ]);

        $image = $request->file('image');
        $image_name = time().".".$image->getClientOriginalExtension();
        $destination = "images/";
        $image->move($destination, $image_name);
        
        $post = Post::find($id);
        $post ->title = $request->title;
        $post ->category_id = $request->category_id;
        $post ->location_id = $request->location_id;
        $post ->description = $request->description;
        $post->image =
        $post->image = $destination.$image_name;
        $post->save();
        return redirect('posts')->with('success', 'Post Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);

        if(auth()->user()->id !==$post->user_id){
            return redirect('/posts')->with('error', 'Unauthorised Page');
        }

        $post->delete();
        return redirect('posts')->with('error', 'Post Removed');
    }
}
